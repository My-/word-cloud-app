package ie.gmit.sw.webcrowler.processors;

import ie.gmit.sw.webcrowler.models.Page;
import ie.gmit.sw.webcrowler.models.QueryWord;
import ie.gmit.sw.webcrowler.publishers.PagePublisher;
import ie.gmit.sw.webcrowler.search.DuckDuckGoSearch;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;

@Slf4j
class PageProcessorTest {
    // root dir
    final String dir = System.getProperty("user.dir");
    // ignore file path
    Path path = Paths.get(dir +"/data/ignorewords.txt");
    // ignore words
    IgnoreWords ignoreWords = IgnoreWords.of(path);
    // fuzzy
    FuzzyPageBooster fuzzy = FuzzyPageBooster.of("fcl/word-score.res");
    // query string
    QueryWord query = QueryWord.of("rust");
    // Duck initial search nodes
    DuckDuckGoSearch startNode = new DuckDuckGoSearch();


    @SneakyThrows
    @Test
    void process() {
        var pagePublisher = PagePublisher.of(query, startNode, ignoreWords);
        var pageProcessor = PageProcessor.of(Flux.from(pagePublisher), fuzzy);
        pageProcessor.getWordPublisher()//.getPagePublisher()
//                .parallel()
//                .runOn(Schedulers.elastic())
//                .log()
                .subscribeOn(Schedulers.elastic())
//                .subscribe(new Printer());
                .subscribe(word -> log.info("onNext(),  {}", word));
//                .subscribe(page -> log.info("onNext(), Page: {}, distance: {}, boost: {}", page.getLink(), page.getDistance(), page.getBoostScore()));

        try {
            pageProcessor.getWordPublisher().timeout(Duration.ofSeconds(20)).then().block();
        } catch (Exception e) {
            log.info("done");
            System.exit(1);
        }
    }

    // print to console
    private class Printer implements Subscriber<Page> {
        private Subscription s;

        @Override
        public void onSubscribe(Subscription s) {
            log.info("Subscribed {}", s);
            this.s = s;
            s.request(1);
        }

        @Override
        public void onNext(Page page) {
            log.info("onNext(), Page: {}, distance: {}, boost: {}", page.getLink(), page.getDistance(), page.getBoostScore());
            s.request(1);
        }

        @Override
        public void onError(Throwable t) {
            log.error("onError: {}", t.getMessage());
        }

        @Override
        public void onComplete() {
            log.info("onCompleate");
        }
    }

}

