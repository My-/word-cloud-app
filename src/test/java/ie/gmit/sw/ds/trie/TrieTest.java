package ie.gmit.sw.ds.trie;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.BinaryOperator;

import static org.junit.jupiter.api.Assertions.*;

class TrieTest {

    @org.junit.jupiter.api.Test
    void insert() {
        var trie = TrieSet.empty();
        var test = List.of(
                "abcd", "abc", "bbc", "abcca"
        );

//        assertAll(() -> );
        trie.addAll(test);

        System.out.println(trie);

        assertAll(
//                () -> assertTrue(trie.contains("abcd"), "precise - abcd"),
//                () -> assertTrue(trie.contains("bbc"), "precise - bbc"),
//                () -> assertTrue(trie.contains("abcca"), "precise - abcca"),
                () -> assertFalse(trie.contains("abca"), "precise - abca"),
                () -> assertFalse(trie.contains("aabca"), "precise - aabca"),
                () -> assertFalse(trie.contains("a"), "precise - a")
        );

        assertAll(
                () -> assertTrue(trie.contains("abcd", 2), "abcd"),
                () -> assertTrue(trie.contains("bbc", 2), "bbc"),
                () -> assertTrue(trie.contains("abcca", 2), "abcca"),
                () -> assertTrue(trie.contains("abca", 2), "abca"),
                () -> assertTrue(trie.contains("abcdef", 2), "abcdef"),
                () -> assertTrue(trie.contains("bbcca", 2), "bbcca"),
                () -> assertTrue(trie.contains("aabca", 2), "aabca"),
                () ->assertFalse(trie.contains("bbccaa", 2), "bbccaa"),
                () -> assertTrue(trie.contains("cba", 2), "cba")
        );


    }

    @Test
    void contains() {
    }

    @Test
    void values() {
        var trie = TrieSet.empty();
        var test = List.of(
                "abcd", "abc", "bbc", "abcca"
        );

        test.forEach(trie::add);
        System.out.println(trie);

        var values = trie.keys();
        System.out.println(values);
    }

    @Test
    void remove() {
        var trie = TrieSet.empty();
        var test = List.of(
                "abcd", "abc", "bbc", "abcca"
        );

        trie.addAll(test);
        System.out.println(trie);

        var values = trie.keys();
        System.out.println(values);

        assertAll(
                // exist
                () -> assertTrue(trie.contains("abcd"), "contains - abcd"),
                () -> assertTrue(trie.contains("bbc"), "contains - bbc"),
                () -> assertTrue(trie.contains("abcca"), "contains - abcca"),
                // remove
                () -> assertTrue(trie.remove("abcd"), "abcd"),
                () -> assertFalse(trie.remove("abcd"), "abcd"),
                () -> assertTrue(trie.remove("abc"), "abc"),
                () -> assertFalse(trie.remove("abc"), "abc"),
                () -> assertFalse(trie.remove("bbcca"), "bbcca"),
                () -> System.out.println(trie.keys())
        );

        System.out.println(trie.keys());
    }

    @Test
    void remove_checkPruning() {
        var trie = TrieSet.empty();
        var test = List.of(
                "abcd", "abc", "bbc", "abcca"
        );

        trie.addAll(test);
        System.out.println(trie);

        var values = trie.keys();
        System.out.println(values);

        assertAll(
                // exist
                () -> assertTrue(trie.contains("abcd"), "abcd"),
                () -> assertTrue(trie.contains("bbc"), "bbc"),
                () -> assertTrue(trie.contains("abc"), "abc"),
                () -> assertTrue(trie.contains("abcca"), "abcca"),
                // remove
                () -> assertTrue(trie.remove("abcd"), "abcd"),
                () -> assertFalse(trie.remove("abcd"), "abcd"),
                () -> assertTrue(trie.remove("abc"), "abc"),
                () -> assertFalse(trie.remove("abc"), "abc"),
                () -> assertFalse(trie.remove("bbcca"), "bbcca")
        );

        System.out.println(trie.keys());
    }

    @Test
    void trieMap_collectKeyValuePairs() {
        var trieMap = TrieMap.empty();
        var test = List.of(
                "abcd", "abc", "bbc", "abcca"
        );

        test.forEach(s -> trieMap.put(s, s.length()));
        System.out.println(trieMap);

        var values = trieMap.collectKeyValuePairs();
        System.out.println(values);

        assertAll(
                // exist
                () -> assertTrue(trieMap.contains("abcd"), "abcd"),
                () -> assertTrue(trieMap.contains("bbc"), "bbc"),
                () -> assertTrue(trieMap.contains("abc"), "abc"),
                () -> assertTrue(trieMap.contains("abcca"), "abcca"),
                // remove
                () -> assertEquals(trieMap.remove("abcd"), 4,"abcd"),
                () -> assertNotEquals(trieMap.remove("abcd"), 4,"abcd"),
                () -> assertEquals(trieMap.remove("abc"), 3, "abc"),
                () -> assertNotEquals(trieMap.remove("abc"), 3, "abc"),
                () -> assertNotEquals(trieMap.remove("bbcca"), 5, "bbcca")
        );

        System.out.println(trieMap.collectKeyValuePairs());
    }

    @Test
    void trie_update() {
        TrieMap<Integer> trieMap = TrieMap.empty();
        var test = List.of(
                "abcd", "abc", "bbc", "abcca"
        );

        test.forEach(s -> trieMap.put(s, s.length()));
        System.out.println(trieMap);

        var values = trieMap.collectKeyValuePairs();
        System.out.println(values);

//        BiFunction<Integer, Integer, Integer> updater = (old, neW) -> old *neW;
        BinaryOperator<Integer> updater = (old, neW) -> old *neW;

        assertAll(
                // exist
                () -> assertTrue(trieMap.contains("abcd"), "abcd"),
                () -> assertTrue(trieMap.contains("bbc"), "bbc"),
                () -> assertTrue(trieMap.contains("abc"), "abc"),
                () -> assertTrue(trieMap.contains("abcca"), "abcca"),
                // values
                () -> assertEquals(trieMap.get("abcd"), 4, "abcd"),
                () -> assertEquals(trieMap.get("bbc"), 3, "bbc"),
                () -> assertEquals(trieMap.get("abc"), 3, "abc"),
                () -> assertEquals(trieMap.get("abcca"), 5, "abcca"),
                () -> assertNotEquals(trieMap.get("abc"), 5, "abcca"),
                () -> assertNotEquals(trieMap.get("abcca"), 3, "abcca"),
                // update
                () -> {
                    trieMap.updateOrInsert("abc", 2, updater);
                    assertEquals(6, trieMap.get("abc"),  "abc * 2");
                },
                () -> {
                    trieMap.updateOrInsert("abcca", 3, updater);
                    assertEquals(15, trieMap.get("abcca"),  "abcca * 3");
                },
                () -> {
                    trieMap.updateOrInsert("abcd", 1, updater);
                    assertEquals(4, trieMap.get("abcd"),  "abcd * 1");
                },
                // not existing
                () -> {
                    trieMap.updateOrInsert("zzz", 5, updater);
                    assertEquals(5, trieMap.get("zzz"), "zzz is set to 5");
                    assertTrue(trieMap.contains("zzz"), "zzz");
                }
        );

        System.out.println(trieMap.collectKeyValuePairs());
    }
}