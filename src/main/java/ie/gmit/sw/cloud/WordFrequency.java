package ie.gmit.sw.cloud;

import ie.gmit.sw.ds.trie.TrieKeyValuePair;

public class WordFrequency {
	private CharSequence word;
	private int frequency;
	private int fontSize = 0;

	public WordFrequency(CharSequence word, int frequency) {
		this.word = word;
		this.frequency = frequency;
	}

	public static WordFrequency of(TrieKeyValuePair<Integer> pair){
		return new WordFrequency(pair.getKey(), pair.getValue());
	}

	public String getWord() {
		return this.word.toString();
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getFrequency() {
		return this.frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public int getFontSize() {
		return this.fontSize;
	}

	public void setFontSize(int size) {
		this.fontSize = size;
	}

	public String toString() {
		return "Word: " + getWord() + "\tFreq: " + getFrequency() + "\tFont Size: " + getFontSize();
	}
}