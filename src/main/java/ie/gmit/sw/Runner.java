package ie.gmit.sw;

import ie.gmit.sw.webcrowler.processors.*;
import ie.gmit.sw.webcrowler.models.QueryWord;
import ie.gmit.sw.webcrowler.publishers.PagePublisher;
import ie.gmit.sw.webcrowler.search.DuckDuckGoSearch;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.nio.file.Paths;
import java.time.Duration;

/**
 * This runner is used then app is run as a jar.
 */
@Slf4j
public class Runner {
    public static final Scheduler SCHEDULER = Schedulers.elastic();

    @SneakyThrows
    public static void main(String[] args) {
        var start = System.nanoTime();
        Hooks.onOperatorDebug();
        Schedulers.enableMetrics();

        // search phrase
        var query = QueryWord.of(args[0]);

        // print working dir
        final String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);

        // ignore file path
        var path = Paths.get(dir +"/res/ignorewords.txt");

        // ignore words
        var ignoreWords = IgnoreWords.of(path);
        ignoreWords.contains("you");

        // fuzzy
        var fuzzy = FuzzyPageBooster.of("res/word-score.fcl");

        // publisher
        var pagePublisher = PagePublisher.of(query, new DuckDuckGoSearch(), ignoreWords);
        var pageProcessor = PageProcessor.of(Flux.from(pagePublisher), fuzzy);
        var wordCloudMaker = WordCloudMaker.of();
        pageProcessor.getWordPublisher()
                .parallel()
                .runOn(Schedulers.elastic())
//                .log()
//                .subscribeOn(Schedulers.elastic())
//                .delaySubscription(Duration.ofSeconds(1))
                .subscribe(wordCloudMaker);
//                .subscribe(page -> log.info("onNext(), Page: {}, distance: {}, boost: {}", page.getLink(), page.getDistance(), page.getBoostScore()));

        try {
            pageProcessor.getWordPublisher()
                    .timeout(Duration.ofSeconds(5))
                    .then()
//                    .take(Duration.ofMinutes(1))
                    .block();
        } catch (Exception e) {
            log.info("done");
        }

        var end = System.nanoTime();
        var time = (end -start) / Math.pow(10, 9);
        System.out.println("time : "+ time);
        System.out.println("total links: "+ App.LINK_COUNTER.get());
        System.out.println("using links: "+ App.USED_PAGE_COUNTER.get());
        System.out.println("words using: "+ App.WORD_COUNTER.get());
        System.out.println("Speed (total): "+ (int)(App.LINK_COUNTER.get() / time *60) +" link/min");
        System.out.println("Speed (using): "+ (int)(App.USED_PAGE_COUNTER.get() / time *60) +" link/min");
        System.exit(0);
    }
}
