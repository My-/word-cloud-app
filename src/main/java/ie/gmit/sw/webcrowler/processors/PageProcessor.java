package ie.gmit.sw.webcrowler.processors;

import ie.gmit.sw.ds.trie.TrieKeyValuePair;
import ie.gmit.sw.legacy.WordProcessor;
import ie.gmit.sw.webcrowler.models.Page;
import ie.gmit.sw.webcrowler.models.QueryWord;
import ie.gmit.sw.webcrowler.models.WordScore;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.Optional;
import java.util.function.*;

/**
 * This class is responsible for processing {@link Page} data and scoring its words
 * according to the {@link WordScore} enum values. This class uses {@link Flux} class
 * instead of implementing {@link org.reactivestreams.Processor}. Even so it acts as a
 * the one who does it.
 */
@Slf4j
public class PageProcessor {
    private static final String SPLIT_ON = " ";
    private final FuzzyPageBooster fuzzyBooster;
    /** {@link org.reactivestreams.Subscriber} can get this value and subscribe to it */
    @Getter
    private final Flux<Page> publisher;

    // private constructor
    private PageProcessor(Publisher<Page> publisher, FuzzyPageBooster fuzzyBooster) {
        this.fuzzyBooster = fuzzyBooster;
        this.publisher = Flux.from(publisher);
    }

    /**
     * Factory method.
     * @param publisher to which this processor should subscribe. (eg. {@link ie.gmit.sw.webcrowler.publishers.PagePublisher})
     * @param fuzzyBooster a fuzzy logic class which calculates "boost" score.
     * @return new this class instance.
     */
    public static PageProcessor of(Flux<Page> publisher, FuzzyPageBooster fuzzyBooster ) {
        return new PageProcessor(publisher, fuzzyBooster);
    }

    /**
     * Get this class publisher as {@link Flux} so {@link org.reactivestreams.Subscriber}
     * could subscribe to it (eg. {@link WordCloudMaker}).
     *
     * @return this class as publisher ({@link Flux})
     */
    public Flux<TrieKeyValuePair<Integer>> getWordPublisher(){
        Predicate<TrieKeyValuePair<Integer>> rightLength = pair -> 2 < pair.getKey().length() && pair.getKey().length() < 13;
        return publisher
                .flatMap(this::fuzzyScore)
                .flatMap(this::scoreWords)
                .filter(rightLength)
                .filter(QueryWord::nonQueryString)
                ;
    }

    /**
     * Adds boost score (creates non-blocking calculation pipe) to the
     * given {@link Page} data.
     *
     * @param page we want calculate boost value.
     * @return same page jus with added boost score.
     */
    public Mono<Page> fuzzyScore(Page page){
        var proceedData = page.getProcessedData();
        // set input values for the fuzzy scorer
        return Mono.just(new FuzzyPageBooster.FuzzyInData())
                .flatMap(fuz -> proceedData.getLinkWords().getQueryWordCount()
                        .map(l -> {
                            fuz.setLingWordFrequency(l.intValue());
                            return fuz;
                        })
                )
                .flatMap(fuz -> Flux.concat(proceedData.getMetaKeywords().getQueryWordCount(), proceedData.getMetaDescription().getQueryWordCount())
                        .reduce(Long::sum)
                        .map(l -> {
                            fuz.setH1h3WordFrequency(l.intValue());
                            return fuz;
                        })
                )
                .flatMap(fuz -> proceedData.getTitle().getQueryWordCount()
                        .map(l -> {
                            fuz.setTitleWordFrequency(l.intValue());
                            return fuz;
                        })
                )
                .flatMap(fuz -> Flux.concat(proceedData.getH1TagsText().getQueryWordCount(), proceedData.getH2h3TagsText().getQueryWordCount())
                        .reduce(Long::sum)
                        .map(l -> {
                            fuz.setH1h3WordFrequency(l.intValue());
                            return fuz;
                        })
                )
                .map(fuzzyBooster::fuzzify) // <-- calculate fuzzy boost value
                .map(d -> {
                    page.setBoostScore(d);
                    return page;
                })
                ;
    }

    /**
     * Score page words:
     *  page -> [page words] -> calculate score for each word -> [scored words]
     *
     * @param page we want to score.
     * @return reactive stream of scored words.
     */
    public Flux<TrieKeyValuePair<Integer>> scoreWords(Page page){
        log.info("Scoring page: {}", page);
        // frequent/near word * word location * fuzzy score
        IntToDoubleFunction boost = i -> i * page.getBoostScore();
        Function<ProcData, Flux<TrieKeyValuePair<Integer>>> mapper = data -> Flux.concat(
                data.getFrequentWords()
                        .map(pair -> {
                            var f = data.getWordScore().andThen(WordScore.FREQUENT_WORD);
                            var score = boost.applyAsDouble(f.applyAsInt(pair.getValue()));
                            return TrieKeyValuePair.of(pair.getKey(), (int)score);
                        }),
                data.getWordsNear()
                        .map(pair -> {
                            var f = data.getWordScore().andThen(WordScore.NEARBY_WORD);
                            var score = boost.applyAsDouble(f.applyAsInt(pair.getValue()));
                            return TrieKeyValuePair.of(pair.getKey(), (int)score);
                        })
        );

        return Flux.concat(
                Mono.just(page.getProcessedData().getLinkWords()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getMetaKeywords()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getMetaDescription()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getTitle()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getH1TagsText()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getH2h3TagsText()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getH4h5TagsText()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getStrongText()).flatMapMany(mapper),
                Mono.just(page.getProcessedData().getParagraphsText()).flatMapMany(mapper)
        );
    }

    /**
     * Gets filter function. This function will take single string and will
     * brake it into multi string reactive stream. Braking string involves
     * string filtering according to provided {@link IgnoreWords} class. As
     * well removing all non valid characters ("[^a-z]").
     *
     * @param queryWord search phrase
     * @param ignoreWords words to ignore
     * @return function for transferring string to word reactive stream.
     */
    public static Function<String, Flux<String>> filterFunction(QueryWord queryWord, IgnoreWords ignoreWords){
        log.debug("filter function: query-word: {}", queryWord);
        return (text) -> {
            if(Objects.isNull(text)){
                return Flux.empty();
            }
            Predicate<String> rightSize = s -> 2 < s.length() && s.length() < 13;
            return Mono.just(text)
                    .map(s -> s.replaceAll("(-|\\.|:|\\/)", SPLIT_ON))
                    .map(queryWord::replace)
                    .map(s -> s.split(SPLIT_ON))
                    .flatMapMany(Flux::fromArray)
                    .map(String::toLowerCase)
                    .map(word -> word.replaceAll("[^a-z]", ""))
                    .filter(rightSize)
                    .filter(ignoreWords::notContains)
                    ;
        };
    }


    /**
     * This inner class holds processed page text.
     */
    @Data
    public static class ProcData {
        private final WordScore wordScore;
        private final Flux<String> text;
        private Flux<TrieKeyValuePair<Integer>> wordsNear;
        private Flux<TrieKeyValuePair<Integer>> frequentWords;
        private Mono<Long> queryWordCount;

        public ProcData (String text, WordScore wordScore, Function<String, Flux<String>> filterTextFunction){
            this.text = filterTextFunction.apply(text);
            this.wordScore = wordScore;
        }

        public Flux<TrieKeyValuePair<Integer>> getWordsNear() {
            return Optional.ofNullable(wordsNear).orElseGet(() -> {
                wordsNear = WordProcessor.getWordsNearFreq(this.text, QueryWord.REPLACEMENT_STRING_TRIE, 0);
                return wordsNear;
            });
        }

        public Flux<TrieKeyValuePair<Integer>> getFrequentWords() {
            return Optional.ofNullable(frequentWords).orElseGet(() -> {
                frequentWords = WordProcessor.getFrequentWords(this.text);
                return frequentWords;
            });
        }

        public Mono<Long> getQueryWordCount() {
            var d = Optional.ofNullable(queryWordCount).orElseGet(() -> {
                queryWordCount = WordProcessor.getQueryWordCount(this.text)
//                .doOnNext(it -> log.info("Query word: {}", it))
                ;
                return queryWordCount;
            });
            log.debug("Query word: {}", d);
            return d;
        }
    }




}
