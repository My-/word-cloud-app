package ie.gmit.sw.webcrowler.processors;

import ie.gmit.sw.webcrowler.models.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import reactor.util.annotation.Nullable;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * This class responsible for fetching web-pages and
 * for scraping text from them.
 */
@Slf4j
@RequiredArgsConstructor(staticName = "of")
public class PageScraper implements Callable<Page.Data> {
    private final Page page;


    @Override
    public @Nullable
    Page.Data call(){
        Document doc = null;
        try {
            doc = Jsoup.connect(page.getLink()).get();
        } catch (IOException e) {
            log.warn("Couldn't get document: {} for page: {}", e.getMessage(), page);
            return null;
        }
//        log.info("Scarping : {}", page);
//        var startTime = System.nanoTime();

        var metaKeywords = doc.select("meta[name=keywords]")
                .stream()
                .filter(Objects::nonNull)
                .findFirst()
                .map(e -> e.attr("content"))
                .orElse(null)
                ;

        var metaDescription = doc.select("meta[name=description]")
                .stream()
                .filter(Objects::nonNull)
                .findFirst()
                .map(e -> e.attr("content"))
                .orElse(null)
                ;

        var title = doc.title();

        var h1Tags = doc.select("h1").text();
        var h2h3Tags = doc.select("h2, h3").text();
        var h4h5Tags = doc.select("h4, h5").text();
        var strong = doc.select("strong, b, i, h6").text();
        var paragraphs = doc.select("p").text();

        var hrefs = doc
                .select("a[href]")
                .stream()
                .map(element -> element.absUrl("href"))
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList())
                ;

        var pageData = Page.Data.newInstance();

        pageData.setMetaKeywords(metaKeywords);
        pageData.setMetaDescription(metaDescription);
        pageData.setTitle(title);
        pageData.setH1TagsText(h1Tags);
        pageData.setH2h3TagsText(h2h3Tags);
        pageData.setH4h5TagsText(h4h5Tags);
        pageData.setStrongText(strong);
        pageData.setParagraphs(paragraphs);
        pageData.setHrefs(hrefs);

//        var endTime = System.nanoTime();
//        var time = String.format("%.0f ms", (endTime -startTime) / Math.pow(10, 6));
//        log.info("Done in: {} - {}", time, page);

        return pageData;
    }

}
