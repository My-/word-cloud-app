package ie.gmit.sw.webcrowler.processors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.jFuzzyLogic.FIS;
//import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

@Slf4j
public class FuzzyPageBooster {
    public static boolean SHOW_FUZZY_CHARTS = false;
    private final FIS fis;

    private FuzzyPageBooster(String fclFilePath){
        this.fis = FIS.load(fclFilePath,true);
    }

    public static FuzzyPageBooster of(String fclFilePath){
        return new FuzzyPageBooster(fclFilePath);
    }

    public static void main(String[] args) {
        var pageBooster = FuzzyPageBooster.of("fcl/word-score.res");
        pageBooster.fuzzify(null);
    }

    /**
     * Fuzzy rule:
     *      - If [word] is in [metadata] And [word] is in [title] Then [word] is [important]
     *
     */
    public double fuzzify(FuzzyInData data) {
        var functionBlock = fis.getFunctionBlock(Function.SCORE_BOOSTER.getName());

        // Set inputs
        fis.setVariable(Inputs.LINK_WORD_FREQUENCY.getName(), data.getLingWordFrequency());
        fis.setVariable(Inputs.META_WORD_FREQUENCY.getName(), data.getMetaWordFrequency());
        fis.setVariable(Inputs.TITLE_WORD_FREQUENCY.getName(), data.getTitleWordFrequency());
        fis.setVariable(Inputs.H1_H3_WORD_FREQUENCY.getName(), data.getH1h3WordFrequency());

        // Evaluate
        fis.evaluate();
        Variable boost = functionBlock.getVariable(Outputs.PAGE_WORD_SCORE_BOOST.getName());
        var res = boost.getLatestDefuzzifiedValue();

        // show charts
//        if(SHOW_FUZZY_CHARTS){
//            JFuzzyChart.get().chart(functionBlock);
//            JFuzzyChart.get().chart(boost, boost.getDefuzzifier(), true);
//        }
//        log.info("fuzzy res: {}, data: {}", res, data);

        return res;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class FuzzyInData {
        private int lingWordFrequency;
        private int metaWordFrequency;
        private int titleWordFrequency;
        private int h1h3WordFrequency;
    }

    // enum for the inputs
    enum Inputs {
        LINK_WORD_FREQUENCY("link"),
        META_WORD_FREQUENCY("metaTag"),
        TITLE_WORD_FREQUENCY("title"),
        H1_H3_WORD_FREQUENCY("h1_h3");

        private String name;
        Inputs(String value){
            this.name = value;
        }
        public String getName(){
            return this.name;
        }
    }

    // enum for outputs
    enum Outputs {
        PAGE_WORD_SCORE_BOOST("boost");

        private String name;
        Outputs(String value){
            this.name = value;
        }
        public String getName(){
            return this.name;
        }
    }

    // enum for function blocks
    enum Function {
        SCORE_BOOSTER("word_booster");

        private String name;
        Function(String value){
            this.name = value;
        }
        public String getName(){
            return this.name;
        }
    }
}
