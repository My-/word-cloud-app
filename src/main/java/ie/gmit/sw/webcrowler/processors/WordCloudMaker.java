package ie.gmit.sw.webcrowler.processors;

import ie.gmit.sw.App;
import ie.gmit.sw.cloud.LogarithmicSpiralPlacer;
import ie.gmit.sw.cloud.WeightedFont;
import ie.gmit.sw.cloud.WordFrequency;
import ie.gmit.sw.ds.trie.TrieKeyValuePair;
import ie.gmit.sw.ds.trie.TrieMap;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BinaryOperator;

/**
 * This class implements {@link Subscriber<TrieKeyValuePair<Integer>>} interface
 * and is last link in programs execution chain. It's responsibility is to
 * create word-cloud pictures. Some parts of code in this class was "adopted"
 * from provided starter code.
 *
 */
@Slf4j
public class WordCloudMaker implements Subscriber<TrieKeyValuePair<Integer>> {
    /** Display word in the words cloud */
    private static final int WORD_CLOUD_WORDS = 70;
    /** Create image every {@code IMAGE_EVERY} words */
    private static final int IMAGE_EVERY = 200;
    /** Prune words buffer queue to the {@code IMAGE_EVERY} amount */
    private static final int PRUNE_KEEP = 500;
    private static final AtomicInteger wordCounter = App.WORD_COUNTER;

    private Comparator<TrieKeyValuePair<Integer>> comparator;
    private final TrieMap<Integer> wordFrequencyList;
    private Subscription subscription;
    private BufferedImage picture;

    private WordCloudMaker(){
        this.comparator = (a, b)-> b.getValue() -a.getValue();
        this.wordFrequencyList = TrieMap.empty();
    }

    /**
     * Factory method
     * @return this instance
     */
    public static WordCloudMaker of(){
        return new WordCloudMaker();
    }

    /**
     * Creates a word-cloud
     * @param fileName for png file.
     */
    public void createWordCloud(CharSequence fileName) {
        var frqList = getFrequency();
        log.debug("Creating Word Cloud from: {} words", frqList.length);
        WordFrequency[] words = new WeightedFont().getFontSizes(frqList);
        Arrays.sort(words, Comparator.comparing(WordFrequency::getFrequency, Comparator.reverseOrder()));

        //Spira Mirabilis
        LogarithmicSpiralPlacer placer = new LogarithmicSpiralPlacer(1000, 800);
        for (WordFrequency word : words) {
            placer.place(word); //Place each word on the canvas starting with the largest
        }

        BufferedImage cloud = placer.getImage();
        File outputfile = new File("data/"+ fileName +".png");
        try {
            ImageIO.write(cloud, "png", outputfile);
        } catch (IOException e) {
            log.error("Could not create image: {}", e.getMessage());
            e.printStackTrace();
        }

        picture = cloud;
    }

    /**
     * Gets last created picture or default which is picture of "null".
     * @return last created picture
     */
    public String getPicture(){
        if(Objects.nonNull(picture)){
            return encodeToString(picture);
        }
        // print null image if no value exist
        return "iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAA3NCSVQICAjb4U/gAAAAIVBMVEX3lB4AAAD+mB8uGwXihxvsjRybXRPCdBd5SA5QLwkbDwNR0+7xAAAAX3pUWHRSYXcgcHJvZmlsZSB0eXBlIEFQUDEAAAiZ40pPzUstykxWKCjKT8vMSeVSAANjEy4TSxNLo0QDAwMLAwgwNDAwNgSSRkC2OVQo0QAFmJibpQGhuVmymSmIzwUAT7oVaBst2IwAAANfSURBVHic7drhcqMgFIZhgoDg/V/wagzIAazaaRrsvs/sj53EIl9QkaNKAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4CKtTeFgc1tsbn+po9+krR+mhzB91WXtys0fo/613n7L8Ch9lVD7avPOE2pX9/iLhHqsN+89YWNMSHizhNeOUmVul1CpcClh6xfpPaF2YXg5kXA+Tn3cfrpJwnkKV9Yu/8yZhM8p/7m58ndJGGl7LmHa/nYJFQkL70uoZz/c5KqLhHM2M87MO1J+IqHOlip2uRa48LpET8HZsmEtFjb55+rUWucTCU02HTttijssLzPqMZ+8h/jdvDwSf+V2+/OBhOIWyR3cTRS3jCF+ZYvlUU8J5V1xvY6T3dXy9ismrG6zukrYDLU3ilp+kRKWi4e7JXykK4qWxYV7JGyceZV0vhVRUsJyRddTwlYNZT4fQwj556Z9SUnJVTGIfSW0riiETc7Y57S3Da/fpoV567QE2hKqdb0T2+kr4TKJ54eqV1ucGGXILqfzf+NBGfKJRGWHfGcJ5cnYnP4mUcDVrYSinZ4TFi3FQTT5x7dOWDSUZvjxryQse5YS/pkx3E3IGB60s+k2IWN40M6m24SM4UE7m24TMoYH7Wy6TcgYHrSz6Tbh/zqGg+hmVjS9YUI5hqnGmgWfV/9bpWA3YfZ07VSXf6yqHxN6mz2rWL5ICVX+DCMl9OunzybySohoR77hFBNmjwCK5yP5A4J0WLitiauPU7SRVd4h/lp6lDWqrL6/lSDDc/FvXP2gPqR2ygcFtZCfBPZw8+HagNqqgXV3dZkxpFNHFBWHvYrrqx+tNyt29vl0poC7f543hrD+xdYrpC3fLMvabb7xUlnLV62XaWr+YtMXhrBV89Y7P73fDtNT3V4T1gdwq8sp4ZkKdVaFf1fC1gjvtPP5hHWLr3m8jp6d4Nocny5h53xuuXqUXrrUlF191UbrXXnxy5jGe05eHLyv39meGMTs1uHUCXBxdnzW6aPswbZJLyYtn/tR/pVWxeCE0Wo3NdqRO2jw8mH6mO+3IThzLaCYy5WYqOVEXP3d3JeYzslbAtno4Qxd3kld6O+76eX97eWe5Ff3CgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACA9A/B2iDd+6QCBgAAAABJRU5ErkJggg==";
    }

    // get frequency array
    private WordFrequency[] getFrequency(){
        var pairs = wordFrequencyList.collectKeyValuePairs();
        var ordered = new PriorityQueue<>(comparator);
        ordered.addAll(pairs);
        return ordered.stream()
                .limit(WORD_CLOUD_WORDS)
                .map(WordFrequency::of)
                .toArray(WordFrequency[]::new)
                ;
    }

    // prun buffer queue to the given size
    private void prune(int keep) {
        var pairs = wordFrequencyList.collectKeyValuePairs();
        if (pairs.size() < keep) { return; }
        var ordered = new PriorityQueue<>(comparator);
        ordered.addAll(pairs);
        synchronized (wordFrequencyList) {
            log.info("Will need to prune: {}", (ordered.size() - keep));
            ordered.stream()
                    .skip(keep)
//                    .peek(it -> log.info("Pruning: {}", it))
                    .map(TrieKeyValuePair::getKey)
                    .forEach(wordFrequencyList::remove)
            ;
        }
    }

    // encode picture as string (provided code)
    private String encodeToString(BufferedImage image) {
        String s = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "png", bos);
            byte[] bytes = bos.toByteArray();

            Base64.Encoder encoder = Base64.getEncoder();
            s = encoder.encodeToString(bytes);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    // decode image string to picture (provided code)
    private BufferedImage decodeToImage(String imageString) {
        BufferedImage image = null;
        byte[] bytes;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            bytes = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    public void onSubscribe(Subscription s) {
        this.subscription = s;
        log.info("Subscribed: {}", subscription);
        subscription.request(5);
    }

    @Override
    public void onNext(TrieKeyValuePair<Integer> pair) {
        var count = wordCounter.incrementAndGet();
        synchronized (wordFrequencyList){
            log.debug("inderting word to word-cloud: {}", pair);
            BinaryOperator<Integer> updater = Integer::sum;
            wordFrequencyList.updateOrInsert(pair.getKey(), pair.getValue(), updater);
        }

        if(count % IMAGE_EVERY == 0){
            this.prune(PRUNE_KEEP);
            createWordCloud("img_"+ count);
        }
        subscription.request(1);
    }

    @Override
    public void onError(Throwable t) {
        log.error("Error: {}", t.getMessage());
        t.printStackTrace();
    }

    @Override
    public void onComplete() {
        createWordCloud("img_"+ wordCounter.get());
        log.info("Cloud is done!");
    }
}
