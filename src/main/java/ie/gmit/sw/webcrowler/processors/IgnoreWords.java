package ie.gmit.sw.webcrowler.processors;

import ie.gmit.sw.ds.trie.Trie;
import ie.gmit.sw.ds.trie.TrieSet;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.BaseStream;

/**
 * This class contains ignore word list which is filled asynchronously.
 * Here is convenient method {@link IgnoreWords#isReady()} to check status.
 * Underneath structure uses custom {@link Trie} implementation. It allows
 * Check words with allowing few character misses (typos, similarly looking words).
 */
@Slf4j
public class IgnoreWords {
    private TrieSet ignoreWords;
    private volatile boolean isReady;
    private final static IgnoreWords instance = new IgnoreWords();

    // private constructor
    private IgnoreWords() {
        this.ignoreWords = TrieSet.empty();
    }

    /**
     * Creates new instance of {@link IgnoreWords} and fills it
     * <strong>asynchronously</strong> with ignore words form the
     * given path.
     * @param ignoreWordsPath source for ignore word file
     * @return newly created instance. Note: ignore word list
     * might not be ready. Check it with {@link IgnoreWords#isReady()}.
     */
    public static IgnoreWords of(Path ignoreWordsPath) {
        log.info("Creating: {}", IgnoreWords.class.getName());
        var instance = new IgnoreWords();
        // ref: https://simonbasle.github.io/2017/10/file-reading-in-reactor
        Flux.using(() -> Files.lines(ignoreWordsPath),
                Flux::fromStream,
                BaseStream::close
        )
                .map(String::toLowerCase)
                .map(s -> s.replaceAll("[^a-z]", ""))
//                .doOnNext(System.out::println)
                .collect(TrieSet::empty, TrieSet::add)
                .subscribeOn(Schedulers.single())
                .subscribe(t-> {
                    instance.ignoreWords = t;
                    instance.isReady = true;
                    log.info("Ignore word list: Done!");
                })
        ;

        return instance;
    }

    /**
     * Checks if ignore list is done reading path.
     * @return true if reading path is done.
     */
    public boolean isReady(){
        return this.isReady;
    }

    /**
     * Checks if ignore list contains given word.
     * @param word given word
     * @return true if word in the ignore list.
     */
    public boolean contains(CharSequence word){
        String msg = null;
        if(!isReady){
            msg = "Using while still filling ignore words. (word: "+  word +")";
        }
        var res = this.ignoreWords.contains(word);
        if(Objects.nonNull(msg)){
            log.warn("{} -> {}", msg, res);
        }
        return res;
    }
    /**
     * Checks if ignore list DO NOT contains given word.
     * @param word  to check.
     * @return {@code true} is word is not in the list of ignore words.
     */
    public boolean notContains(CharSequence word){
        return !this.contains(word);
    }

    /**
     * Checks if ignore list contains given word with allowing
     * character given misses.
     * @param word given word
     * @param allowedMisses allowed misses
     * @return true if word in the ignore list or is in the list
     * with given amount of character misses.
     */
    public boolean containsWithMisses(CharSequence word, int allowedMisses){
        String msg = null;
        if(!isReady){
            msg = "Using while still filling ignore words. (word: "+  word +", allowedMisses: "+ allowedMisses +")";
        }
        var res = this.ignoreWords.contains(word, allowedMisses);
        if(Objects.nonNull(msg)){
            log.warn("{} -> {}", msg, res);
        }
        return res;
    }
}
