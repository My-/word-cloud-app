package ie.gmit.sw.webcrowler.publishers;

import ie.gmit.sw.App;
import ie.gmit.sw.Runner;
import ie.gmit.sw.ds.trie.TrieMap;
import ie.gmit.sw.webcrowler.models.Page;
import ie.gmit.sw.webcrowler.models.QueryWord;
import ie.gmit.sw.webcrowler.processors.IgnoreWords;
import ie.gmit.sw.webcrowler.processors.PageProcessor;
import ie.gmit.sw.webcrowler.processors.PageScraper;
import ie.gmit.sw.webcrowler.search.SearchEngine;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * This class implements reactive streams {@link Publisher<Page>} interface.
 * By doing so it allows {@link Subscriber<Page>} to subscribe to it and
 * push {@link Page} data to that subscribed {@link Subscriber<Page>}. This
 * {@link Publisher<Page>} doing it asynchronously (non-blocking) on subscriber demand.
 * To achieve this I used {@link BlockingQueue<Page>} as my buffer storage. To avoid long
 * runs {@link PagePublisher#MAX_RUN_TIME_SEC} variable sets how long publisher should
 * run. After that time passes this {@link PagePublisher} class {@link Runnable#run()}
 * method thread is interrupted and no more {@link Page} data request send to {@link PageScraper}
 * and because of it no more scraped {@link Page}s added to the {@link PagePublisher#proceedPages}
 * queue. So subscriber will eventually timeout as no more data will be available.
 */
@Slf4j
public class PagePublisher implements Publisher<Page>, Runnable {
    /** Value for the time in second how long this publisher should provide new data */
    private static final int MAX_RUN_TIME_SEC = 60;
    /** Value for the buffer queue poll timeout */
    private static final int POLL_TIMEOUT = 3000;
    /** Amount of data in the queue  */
    private static final int PAGE_QUEUE_MAX = 500;
    private static final Scheduler SCHEDULER = Runner.SCHEDULER;
    private static final Scheduler SCRAPER_FEEDER_SCEDULER = Schedulers.single();
    /** Regular expresion for page part recognition inside the url link */
    private static final Pattern RE = Pattern.compile("(?<=(://))(?:(?!(#|\\?)).)*");

    // counters
    private static final AtomicInteger countdown = new AtomicInteger();
    private static final AtomicInteger totalLinksCounter = App.LINK_COUNTER;
    private static final AtomicInteger processedPagesCounter = App.USED_PAGE_COUNTER;

    private final TrieMap<Integer> visitedPages;
    private final BlockingQueue<Page> proceedPages;
    private final BlockingQueue<Page> todoPageQueue;
    /** this class as {@link Flux} */
    private final Flux<Page> publisher;
    private final Function<String, Flux<String>> filterTextFunction;
    /** Search phrase */
    @Getter
    private final QueryWord query;
    /** This publisher start time */
    @Getter
    private long startTime;
    /** Value which declares how many search phrases should be in the page
     * to allow that page links be added to the {@link this#todoPageQueue} */
    @Getter @Setter
    private int minInPageKeywords = 3;
    /** This class subscriber */
    private Subscriber<? super Page> subscriber;

    // private constructor
    private PagePublisher(QueryWord query, SearchEngine<List<String>> startNode, IgnoreWords ignoreWords) throws Exception {
        this.query = query;
        this.visitedPages = TrieMap.empty();
        this.proceedPages = new LinkedBlockingDeque<>(PAGE_QUEUE_MAX);
        this.todoPageQueue = new LinkedBlockingDeque<>(PAGE_QUEUE_MAX);
        this.filterTextFunction = PageProcessor.filterFunction(query, ignoreWords);
        startNode
                .search(query.getQuery(), 10)
                .stream()
                .filter(Objects::nonNull)
                .map(link -> Page.of(link, 1, filterTextFunction))
                .peek(it -> totalLinksCounter.incrementAndGet())
                .forEach(this.todoPageQueue::offer)
                ;
        // create publisher
        this.publisher = Flux.create(PagePublisher.this::handleFluxSink);
    }

    /**
     * Factory method.
     * @param query search phrase
     * @param startNode starting links provider.
     * @param ignoreWords word to ignore
     * @return new instance of this class.
     * @throws Exception if things goes wrong.
     */
    public static PagePublisher of(QueryWord query, SearchEngine<List<String>> startNode, IgnoreWords ignoreWords) throws Exception {
        return new PagePublisher(query, startNode, ignoreWords);
    }

    /**
     * Method which handles sink (emits {@link Page}s to {@link Subscriber<Page>})
     * @param sink sink
     */
    private void handleFluxSink(FluxSink<Page> sink) {
        // ref: https://www.baeldung.com/flux-sequences-reactor
        SCHEDULER.schedule(() -> sink.onRequest(requested -> {
                    Page page = null;
                    try {
                        for (int i = 0; i < requested; i++) {
                            page = this.proceedPages.poll(POLL_TIMEOUT, TimeUnit.MILLISECONDS);
                            if (Objects.isNull(page)) {
                                log.warn("..time out.. {}", countdown.get());
                                log.info(getStats());
                                if(countdown.incrementAndGet() > 3){
                                    log.info("sending complete event");
                                    sink.complete();
                                    subscriber.onComplete();
//                                    SCHEDULER.dispose();
//                                    Thread.currentThread().interrupt();
                                }
                                i--;
                                continue;
                            }
                            sink.next(page);
                            processedPagesCounter.incrementAndGet();
//                            log.info("sinked: {}", page);
                        }
                    } catch (InterruptedException e) {
                        log.error("Page poll was interrupted: {}", e.getMessage());
                        sink.error(e);
                    }
                })

        );

    }

    /**
     * Gets stats about running application.
     * @return JSON like string containing stats
     */
    public String getStats(){
        return "\n{" +
                "\n\t\"total links\":  " + totalLinksCounter.get() +
                ",\n\t\"pulled links\": " + processedPagesCounter.get() +
                ",\n\t\"todo buffer\": " + todoPageQueue.size() +
                ",\n\t\"done buffer\": " + proceedPages.size() +
                "\n}";
    }

    @Override
    public void subscribe(Subscriber<? super Page> s) {
        this.subscriber = s;
        this.publisher.subscribe(s);
        SCRAPER_FEEDER_SCEDULER.schedule(this);
        this.startTime = SCRAPER_FEEDER_SCEDULER.now(TimeUnit.SECONDS);
//        this.run();
    }

    @Override
    public void run() {
        for(var page = todoPageQueue.poll(); Objects.nonNull(page); page = getNext() ){
            var nowTime = SCRAPER_FEEDER_SCEDULER.now(TimeUnit.SECONDS);
            if((nowTime -startTime) > MAX_RUN_TIME_SEC){
                log.info("Closing feeder...");
                SCRAPER_FEEDER_SCEDULER.dispose();
                Thread.currentThread().interrupt();
            }
//            log.info("processing: {}", page);
            var n = processedPagesCounter.get();
            if(n != 0 && n % 100 == 0){
                log.info(getStats());
            }
            processToQueue(page);
        }
    }

    // helper for the run for loop
    private Page getNext(){
        try {
            return todoPageQueue.poll(POLL_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            log.error("Cant get next page from the queue: {}", e.getMessage());
            return null;
        }
    }

    // process and then put to the processed page queue
    private void processToQueue(Page page){
        if(page.getLink().isEmpty()){
//            log.warn("Link is empty: {}", page);
            return;
        }
        if(isUsed(page.getLink())){
//            log.info("Page was already used: {}", page);
            return; // dont process if link was already used
        }
        var scraper = PageScraper.of(page);
        Mono.fromCallable(scraper)
                .subscribeOn(Schedulers.elastic())
                .subscribe(data -> {
                    if(Objects.nonNull(data)){
                        page.setData(data);
                        proceedPages.offer(page);
                        // if page query word count more then n add its link to queue
                        Flux.concat(
                                page.getProcessedData().getLinkWords().getQueryWordCount(),
                                page.getProcessedData().getMetaKeywords().getQueryWordCount(),
                                page.getProcessedData().getMetaDescription().getQueryWordCount(),
                                page.getProcessedData().getTitle().getQueryWordCount(),
                                page.getProcessedData().getH1TagsText().getQueryWordCount(),
                                page.getProcessedData().getH2h3TagsText().getQueryWordCount(),
                                page.getProcessedData().getH4h5TagsText().getQueryWordCount(),
                                page.getProcessedData().getStrongText().getQueryWordCount(),
                                page.getProcessedData().getParagraphsText().getQueryWordCount()
                        )
                                .reduce(Long::sum)
                                .doOnNext(it -> log.debug("Total query words [{}] at {}", it, page))
                                .filter(sum -> sum >= minInPageKeywords)
                                .subscribe(sum -> {
                                    addToPagesToProcessQueue(data.getHrefs(), page.getDistance()+1);
                                });
                    }
                });
    }

    // checks if link is used a lot
    private boolean isUsed(String link){
        var ma = RE.matcher(link);
        String domain = "";
        if(ma.find()){
            domain = ma.group(0).toLowerCase().replaceAll("[^a-z]", "");
        }
        else {
            log.warn("Doggy link: {}", link);
            return true;
        }

        synchronized (visitedPages){
            var value = visitedPages.get(domain);
            if(Objects.nonNull(value)){
                visitedPages.put(domain, value +1);
                if(value > 0){
                    return true;
                }
            }
            else {
                visitedPages.put(domain, 1);
            }
        }
        return false;
    }

    // adds links to `pages to process` queue
    private void addToPagesToProcessQueue(List<String> links, int nodeDept){
        links.stream()
                .map(link -> Page.of(link, nodeDept, filterTextFunction))
                .peek(it -> totalLinksCounter.incrementAndGet())
                .forEach(todoPageQueue::offer);
    }
}
