package ie.gmit.sw.webcrowler.search;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Contract implementation for {@link SearchEngine} using {@code DuckDuckGo} as
 * a search provider.
 */
@Slf4j
public class DuckDuckGoSearch implements SearchEngine<List<String>> {
    private static final String DUCK_URL = "https://duckduckgo.com/html/?q=";

    @Override
    public List<String> search(String query, int limit) throws IOException {
        return jSoupe(query);
//        return dummy();
    }

    // dummy results for "rust" search
    private List<String> dummy(){
        return List.of(
                "https://www.rust-lang.org/",
                "https://rust.facepunch.com/",
                "https://en.wikipedia.org/wiki/Rust_(video_game)",
                "https://twitter.com/playrust",
                "https://rust.fandom.com/",
                "https://github.com/rust-lang/rust",
                "https://www.facebook.com/RustFacepunchGame/",
                "https://www.reddit.com/r/rust/",
                "https://rust.gamepedia.com/",
                "https://rust-in.ru/",
                "https://online_rust.gamestores.ru/"
        );
    }

    // puling results from the web
    private List<String> jSoupe(String query) throws IOException {
        List<String> l = Jsoup.connect(DUCK_URL + query)
                .get()
                .getElementById("links")
                .getElementsByClass("result")
                .select("a[href]")
                .stream()
                .map(element -> element.absUrl("href"))
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList())
                ;
        log.info("lvl 1 link: {}", l);

        return l;
    }
}
