package ie.gmit.sw.webcrowler.search;

/**
 * Interface to declare contract for search engine contract
 * @param <R>
 */
public interface SearchEngine <R>{
    /**
     *
     * @param query search phrase
     * @param limit result limit
     * @return results
     * @throws Exception if thing goes wrong
     */
    public R search(String query, int limit) throws Exception;
}
