package ie.gmit.sw.webcrowler.models;

import java.util.function.IntUnaryOperator;

/**
 * This enum holds scoring values wor the words in the page.
 * for the scoring values I decided to use every second prime
 * number (ne real reason). This class as well implements an
 * {@link IntUnaryOperator} functional interface. So it can be
 * used as function too. I believe that is quite awesome.
 */
public enum WordScore implements IntUnaryOperator { // <- functional interface :)
    LINK_TEXT(53),          // in the url
    META_KEYWORD(43),       // "meta[name=keywords]"
    META_DESCRIPTION(37),   // "meta[name=description]"
    TITLE(29),              // title
    HEADER_1(19),           // words inside h1 tags
    HEADERS_2_3(13),        // h2, h3 tags
    HEADERS_4_5(7),         // h4, h5 tags
    HIGHLIGHTED(3),         // strong, bold, underline, h6
    PARAGRAPH(1),           // paragraph text

    FREQUENT_WORD_MAX_VALUE(30),    // max value for frequent word, used by normalization function
    FREQUENT_WORD(1),               // if word from frequent list
    NEARBY_WORD(2),                 // if word from near word list
    ;

    int value;
    WordScore(int value){
        this.value = value;
    }
    public int getValue(){
        return value;
    }
    @Override
    public int applyAsInt(int i) {
        return i * value;
    }
}
