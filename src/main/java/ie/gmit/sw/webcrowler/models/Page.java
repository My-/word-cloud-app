package ie.gmit.sw.webcrowler.models;

import ie.gmit.sw.webcrowler.processors.PageProcessor;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * This is Data Transfer Object (DTO). It contains
 * two related inner classes {@link Page.Data} and
 * {@link Page.ProcessedData}.
 */
@Slf4j
@lombok.Data
@RequiredArgsConstructor(staticName = "of")
public class Page {
    /** Page link address */
    @Getter
    private final String link;
    /** Distance from root node */
    @Getter
    private final int distance;
    /** Function to be used to filter text */
    private final Function<String, Flux<String>> filterTextFunction;
    /** Scraped page data */
    @Setter @Getter
    private Page.Data data;
    /** Processed pages scraped data */
    @Setter(AccessLevel.PRIVATE)
    private Page.ProcessedData processedData;
    /** Page boost score */
    @Setter @Getter
    private double boostScore = 1.0;

    /**
     * Gets processed data. It initializes it on first run
     * (dynamic programming).
     * @return instance of processed data.
     */
    public ProcessedData getProcessedData(){
        var d =  Optional.ofNullable(processedData)
                .orElseGet(() -> {
                    data.setLink(link);
                    processedData = Page.ProcessedData.of(data, filterTextFunction);
                    return processedData;
                });
        log.debug("getting Processed data: {}, filterTextFunction: {}", d, filterTextFunction);

        return d;
    }

    @Override
    public String toString() {
        return "Page{" +
                "distance=" + distance +
                ", boostScore=" + boostScore +
                ", link='" + link + '\'' +
                '}';
    }

    /**
     * Inner {@link Page} class. It is used to store
     * scraped web-page data.
     */
    @lombok.Data
    @NoArgsConstructor(staticName = "newInstance")
    public static class Data {
        private String link;
        private String metaKeywords;
        private String metaDescription;
        private String title;
        private String h1TagsText;
        private String h2h3TagsText;
        private String h4h5TagsText;
        private String strongText;
        private String paragraphs;
        private List<String> hrefs;
    }

    /**
     * Inner {@link Page} class. It is used to store
     * processed {@link Page.Data} data. Note: this class
     * fields has only public getters. By calling get method
     * it internally triggers (if needed) related setter that way
     * value is calculated only if it is needed (dynamic programming).
     */
    @lombok.Data
    @Setter(AccessLevel.PRIVATE)
    @RequiredArgsConstructor(staticName = "of")
    public static class ProcessedData {
        private final Page.Data data;
        private final Function<String, Flux<String>> filterTextFunction;

        private PageProcessor.ProcData linkWords;
        private PageProcessor.ProcData metaKeywords;
        private PageProcessor.ProcData metaDescription;
        private PageProcessor.ProcData title;
        private PageProcessor.ProcData h1TagsText;
        private PageProcessor.ProcData h2h3TagsText;
        private PageProcessor.ProcData h4h5TagsText;
        private PageProcessor.ProcData strongText;
        private PageProcessor.ProcData paragraphsText;

        public PageProcessor.ProcData getLinkWords() {
            var d =  Optional.ofNullable(linkWords).orElseGet(() -> {
                linkWords = new PageProcessor.ProcData(
                        data.getLink(), WordScore.LINK_TEXT, filterTextFunction
                );
                return linkWords;
            });
            log.debug("Processed link data: {}, filterTextFunction: {}", d, filterTextFunction.apply("hello"));
            return d;
        }

        public PageProcessor.ProcData getMetaKeywords() {
            return Optional.ofNullable(metaKeywords).orElseGet(() -> {
                metaKeywords = new PageProcessor.ProcData(
                        data.getMetaKeywords(), WordScore.META_KEYWORD, filterTextFunction
                );
                return metaKeywords;
            });
        }

        public PageProcessor.ProcData getMetaDescription() {
            return Optional.ofNullable(metaDescription).orElseGet(() -> {
                metaDescription = new PageProcessor.ProcData(
                        data.getMetaDescription(), WordScore.META_DESCRIPTION, filterTextFunction
                );
                return metaDescription;
            });
        }

        public PageProcessor.ProcData getTitle() {
            return Optional.ofNullable(title).orElseGet(() -> {
                title = new PageProcessor.ProcData(
                        data.getTitle(), WordScore.TITLE, filterTextFunction
                );
                return title;
            });
        }

        public PageProcessor.ProcData getH1TagsText() {
            return Optional.ofNullable(h1TagsText).orElseGet(() -> {
                h1TagsText = new PageProcessor.ProcData(
                        data.getH1TagsText(), WordScore.HEADER_1, filterTextFunction
                );
                return h1TagsText;
            });
        }

        public PageProcessor.ProcData getH2h3TagsText() {
            return Optional.ofNullable(h2h3TagsText).orElseGet(() -> {
                h2h3TagsText = new PageProcessor.ProcData(
                        data.getH2h3TagsText(), WordScore.HEADERS_2_3, filterTextFunction
                );
                return h2h3TagsText;
            });
        }

        public PageProcessor.ProcData getH4h5TagsText() {
            return Optional.ofNullable(h4h5TagsText).orElseGet(() -> {
                h4h5TagsText = new PageProcessor.ProcData(
                        data.getH4h5TagsText(), WordScore.HEADERS_4_5, filterTextFunction
                );
                return h4h5TagsText;
            });
        }

        public PageProcessor.ProcData getStrongText() {
            return Optional.ofNullable(strongText).orElseGet(() -> {
                strongText = new PageProcessor.ProcData(
                        data.getStrongText(), WordScore.HIGHLIGHTED, filterTextFunction
                );
                return strongText;
            });
        }

        public PageProcessor.ProcData getParagraphsText() {
            return Optional.ofNullable(paragraphsText).orElseGet(() -> {
                paragraphsText = new PageProcessor.ProcData(
                        data.getParagraphs(), WordScore.PARAGRAPH, filterTextFunction
                );
                return paragraphsText;
            });
        }
    }


}
