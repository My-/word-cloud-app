package ie.gmit.sw.webcrowler.models;

import ie.gmit.sw.ds.trie.TrieKeyValuePair;
import ie.gmit.sw.ds.trie.TrieSet;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.function.Function;

/**
 * This class contains all query word (search phrase) related functionality.
 * Idea is to replace search phrase with {@link QueryWord#REPLACEMENT_STRING} string
 * which is hopefully is random enough to not occur often in the text. This is
 * done to cope with multi word search queries.
 */
@Data
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class QueryWord {
    /** String to be used as replacement of query word */
    public static final String REPLACEMENT_STRING = " gfdszzlp "; // some random string hope not gonna appear in the text
    /** Replacement string in the {@link TrieSet} ds */
    public static final TrieSet REPLACEMENT_STRING_TRIE = TrieSet.of(REPLACEMENT_STRING.trim());

    private final String query;
    @ToString.Exclude
    private final TrieSet trieWord;

    /**
     * Factory method for creating instances of this class.
     *
     * @param query search phrase
     * @return new this class instance.
     */
    public static QueryWord of(String query){
        Function<String, String> filter = s -> s.toLowerCase().replaceAll("[^a-z]", "");
        return new QueryWord(query.toLowerCase(), TrieSet.of(filter.apply(query)));
    }

    /**
     * Replaces all occupancies of the search phrase in
     * the given text with {@link QueryWord#REPLACEMENT_STRING}.
     *
     * @param text were search phrases should be replaced.
     * @return string with replaced search phrases.
     */
    public String replace(String text){
        return text.replaceAll(query, REPLACEMENT_STRING);
    }

    /**
     * Checks if given word is not a {@link QueryWord#REPLACEMENT_STRING}.
     *
     * @param word to check against
     * @return {@code true} if not match.
     */
    public static boolean nonQueryString(String word){
        return !REPLACEMENT_STRING_TRIE.contains(word.toLowerCase(), 0);
    }

    /**
     * Checks if given word is not a {@link QueryWord#REPLACEMENT_STRING}.
     *
     * @param word to check against
     * @return {@code true} if not match.
     */
    public static boolean nonQueryString(TrieKeyValuePair<Integer> word){
        return !REPLACEMENT_STRING_TRIE.contains(word.getKey(), 0);
    }

    /**
     * Checks if given word is a {@link QueryWord#REPLACEMENT_STRING}.
     *
     * @param word to check against
     * @return {@code true} if matches it.
     */
    public static boolean isQueryString(String word) {
        return REPLACEMENT_STRING_TRIE.contains(word.toLowerCase(), 0);
    }
}
