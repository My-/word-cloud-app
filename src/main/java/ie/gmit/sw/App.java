package ie.gmit.sw;

import ie.gmit.sw.webcrowler.models.QueryWord;
import ie.gmit.sw.webcrowler.processors.FuzzyPageBooster;
import ie.gmit.sw.webcrowler.processors.IgnoreWords;
import ie.gmit.sw.webcrowler.processors.PageProcessor;
import ie.gmit.sw.webcrowler.processors.WordCloudMaker;
import ie.gmit.sw.webcrowler.publishers.PagePublisher;
import ie.gmit.sw.webcrowler.search.DuckDuckGoSearch;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.core.scheduler.Schedulers;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This is entry point for the whole word-cloud-creator app.
 *
 */
@Slf4j
public class App {
    /** Total word counter receive by {@link WordCloudMaker} */
    public static final AtomicInteger WORD_COUNTER = new AtomicInteger();
    /** Total link counter: all links eve pruned, but NOT including those who was in the low scored pages*/
    public static final AtomicInteger LINK_COUNTER = new AtomicInteger();
    /** total counter of the pages which text was used for the word-cloud creation */
    public static final AtomicInteger USED_PAGE_COUNTER = new AtomicInteger();
    public static final String USER_DIR = System.getProperty("user.dir");
    public static final String IGNORE_WORDS_FILE = "/res/ignorewords.txt";
    public static final String FUZZY_FILE = "res/word-score.fcl";

    private final FuzzyPageBooster fuzzy;
    private final IgnoreWords ignoreWords;
    private  WordCloudMaker wordCloudMaker;
    private boolean isRunning;
    private int minInPageKeywords;

    // private constructor
    private App(int minInPageKeywords) {
        this.minInPageKeywords = minInPageKeywords;
        Hooks.onOperatorDebug();
        Schedulers.enableMetrics();
        var path = Paths.get(USER_DIR +IGNORE_WORDS_FILE);
        this.ignoreWords = IgnoreWords.of(path);
        this.fuzzy = FuzzyPageBooster.of(FUZZY_FILE);
    }

    /**
     * Factory method.
     * @param minInPageKeywords minimum search phrases in the page to be
     *                          accepted for processing.
     * @return
     */
    public static App of(int minInPageKeywords){
        log.info("User directory: {}", USER_DIR);
        return new App(minInPageKeywords);
    }

    public void search(String searchWord) {
        if(isRunning){
            log.error("Another instance is running! Wait until its done!");
            return;
        }
        isRunning = true;
        USED_PAGE_COUNTER.set(0);
        LINK_COUNTER.set(0);
        WORD_COUNTER.set(0);
        var start = System.nanoTime();

        var query = QueryWord.of(searchWord);
        PagePublisher pagePublisher = null;
        try {
            pagePublisher = PagePublisher.of(query, new DuckDuckGoSearch(), ignoreWords);
        } catch (Exception e) {
            log.error("Page publisher cant be created: {}", e.getMessage());
            e.printStackTrace();
            return;
        }
        var pageProcessor = PageProcessor.of(Flux.from(pagePublisher), fuzzy);
        this.wordCloudMaker = WordCloudMaker.of();

        pagePublisher.setMinInPageKeywords(this.minInPageKeywords);

        pageProcessor.getWordPublisher()
                .parallel()
                .runOn(Schedulers.elastic())
//                .subscribeOn(Schedulers.elastic())
//                .delaySubscription(Duration.ofSeconds(1))
//                .timeout(Duration.ofSeconds(20))
                .subscribe(wordCloudMaker);
        try {
            pageProcessor.getWordPublisher()
                    .timeout(Duration.ofSeconds(20))
                    .take(Duration.ofMinutes(1))
//                    .subscribe();
                    .then()
                    .block()
                    ;
        } catch (Exception e) {
            log.info("done running...");
        }

        var end = System.nanoTime();
        var time = (end -start) / Math.pow(10, 9);
        System.out.println("time : "+ time);
        System.out.println("total links: "+ LINK_COUNTER.get());
        System.out.println("using links: "+ USED_PAGE_COUNTER.get());
        System.out.println("words using: "+ WORD_COUNTER.get());
        System.out.println("Speed (total): "+ (int)(LINK_COUNTER.get() / time *60) +" link/min");
        System.out.println("Speed (using): "+ (int)(USED_PAGE_COUNTER.get() / time *60) +" link/min");
        isRunning = false;
    }

    /**
     * Getter for {@link WordCloudMaker} instance used by the app
     * @return instance.
     */
    public WordCloudMaker getWordCloudMaker(){
        return wordCloudMaker;
    }

    /**
     * Get running app stats.
     * @return running app stats.
     */
    public String getStats(){
        return String.format("{\n\"totalLinkCounter\": %d," +
                        "\n\"processedPageCounter\": %d," +
                        "\n\"wordCounter\": %d" +
                        "\n}",
                LINK_COUNTER.get(), USED_PAGE_COUNTER.get(), WORD_COUNTER.get()
        );
    }
}
