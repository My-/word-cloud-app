package ie.gmit.sw.legacy;

import ie.gmit.sw.ds.trie.Trie;
import ie.gmit.sw.ds.trie.TrieKeyValuePair;
import ie.gmit.sw.ds.trie.TrieMap;
import ie.gmit.sw.ds.trie.TrieSet;
import ie.gmit.sw.webcrowler.models.QueryWord;
import ie.gmit.sw.webcrowler.models.WordScore;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.util.*;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;

/**
 * NOTE: This is legacy code. It is here for history reasons.
 * NOTE: this code is still in use.
 *
 * This class is one ot those "utility" classes. It need an upgrade
 * or be replaced.
 *
 * This class main responsibility to find words near the search word
 * {@link QueryWord}. Find frequent words. Note: frequent words are normalized
 * by {@link WordProcessor#getFrequentWords(Flux)} method logic.
 *
 * @deprecated for removal or replaced.
 */
@Slf4j
@Data
public class WordProcessor {
    private final TrieMap<Integer> importantWords = TrieMap.empty();
    private CharSequence last = "";

    public WordProcessor add(CharSequence word){
        var wordValue = importantWords.get(word);
        if(Objects.nonNull(wordValue)){
            importantWords.put(word, wordValue +1);
        }
        else {
            importantWords.put(word, 1);
        }

        last = word;
        return this;
    }

    /**
     * ToDo: Needs remove. used in legacy code only
     * Find words neat the keyword
     *
     * @param wordPublisher
     * @param word
     * @param allowedMisses
     * @return
     */
    public static Flux<TrieKeyValuePair<Integer>> getWordsNearFreq(Flux<String> wordPublisher,
                                                                   QueryWord word,
                                                                   int allowedMisses
    ){
        return wordPublisher
//                .filter(Objects::nonNull)
                .filter(s -> s.length() > 2)
//                .filter(s -> s.length() < 13)
//                .doOnNext(System.out::println)
                .reduce(new WordProcessor(), (r, s) -> {
                    // check prev word
                    if(word.getTrieWord().contains(r.getLast(), allowedMisses)){
                        // then current (s) is after the query word
                        r.add(s);
                    }
                    // check current word
                    else if(word.getTrieWord().contains(s, allowedMisses)){
                        // then current word is the query word
                        var last = r.getLast();
                        r.add(last);
                    }
                    r.setLast(s);

                    return r;
                })
                .map(WordProcessor::getImportantWords)
                .map(TrieMap::collectKeyValuePairs)
                .doOnNext(it -> System.out.println("--> "+ it.size()))
                .flatMapMany(Flux::fromIterable)
//                .doOnNext(System.out::println)
                ;
    }

    /**
     * ToDo: Needs rework, used in PageProcessor
     * Find words neat the keyword
     *
     * @param wordPublisher
     * @param word
     * @param allowedMisses
     * @return
     */
    public static Flux<TrieKeyValuePair<Integer>> getWordsNearFreq(Flux<String> wordPublisher,
                                                                   TrieSet word,
                                                                   int allowedMisses
    ){

        return wordPublisher
//                .filter(Objects::nonNull)
                .filter(s -> s.length() > 2)
//                .filter(s -> s.length() < 13)
//                .doOnNext(it -> log.info("-- {}", it))
                .reduce(new WordProcessor(), (r, s) -> {
                    // check prev word
                    if(word.contains(r.getLast(), allowedMisses)){
                        // then current (s) is after the query word
                        r.add(s);
                    }
                    // check current word
                    else if(word.contains(s, allowedMisses)){
                        // then current word is the query word
                        var last = r.getLast();
                        r.add(last);
                    }
                    r.setLast(s);

                    return r;
                })
                .map(WordProcessor::getImportantWords)
                .map(TrieMap::collectKeyValuePairs)
                .flatMapMany(Flux::fromIterable)
                .filter(pair -> pair.getKey().length() > 0)
                .doOnNext(it -> log.debug("get-words-near-out: {}", it))
                ;
    }

    /**
     * Get frequent words. It uses normalization technique to keep pages most
     * frequent word at same value as other pages.
     *
     * @param wordPublisher
     * @return
     */
    public static Flux<TrieKeyValuePair<Integer>> getFrequentWords(Flux<String> wordPublisher){
        final var limit = 20;

        return wordPublisher
                .doOnNext(it -> log.debug("get-frequent-words-in {}", it))
                .filter(QueryWord::nonQueryString)
                .reduce(TrieMap.empty(), (TrieMap<Integer> trie, String word) -> {
                    trie.updateOrInsert(word, 1, Integer::sum);
                    return trie;
                })
                .map(Trie::collectKeyValuePairs)
                .flatMapMany(Flux::fromIterable)
//                .log()
                .filter(it -> it.getValue() > 20)
                .sort((a, b) -> b.getValue() -a.getValue())
                .take(limit)
                .checkpoint("Before Collecting.")
                .collect(Collectors.toCollection(() -> new TreeSet<>((a, b) -> b.getValue() -a.getValue())))
                .flatMapMany(q -> {
//                    log.info("normalization got: {}", q);
                    if(q.isEmpty()){
                        return Flux.empty();
                    }
                    if(q.size() == 1){
                        var e = q.first();
                        int half = WordScore.FREQUENT_WORD_MAX_VALUE.getValue() /2;
                        return Flux.just(TrieKeyValuePair.of(e.getKey(), half));
                    }
                    // https://stats.stackexchange.com/a/70807/279946
                    var xMin = 0;
                    var xMax = q.first().getValue();
                    DoubleUnaryOperator normalized = d -> ((d -xMin)/(xMax -xMin) * WordScore.FREQUENT_WORD_MAX_VALUE.getValue());

                    var newList = q.stream()
                            .map(pair -> TrieKeyValuePair.of(pair.getKey(), (int)Math.ceil(normalized.applyAsDouble(pair.getValue()))))
                            .collect(Collectors.toList());

                    log.info("Normalization:\nin: {},\nout: {}", q, newList);

                    return Flux.fromIterable(newList);
                })
//                .log()
//                .filter(important)
                ;
    }

    /**
     * Count query words
     *
     * @param wordPublisher
     * @return
     */
    public static Mono<Long> getQueryWordCount(Flux<String> wordPublisher) {
        return wordPublisher
                .filter(QueryWord::isQueryString)
                .count()
//                .doOnNext(it -> System.out.print((it > 0 ? "("+it+")" : ""))) // search regex: "\(([1-9].)\)"
                ;
    }


}
