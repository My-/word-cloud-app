package ie.gmit.sw.legacy;

import ie.gmit.sw.ds.trie.Trie;
import ie.gmit.sw.ds.trie.TrieKeyValuePair;
import ie.gmit.sw.ds.trie.TrieMap;
import ie.gmit.sw.webcrowler.models.QueryWord;
import ie.gmit.sw.webcrowler.processors.FuzzyPageBooster;
import ie.gmit.sw.webcrowler.processors.IgnoreWords;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * NOTE: This is legacy code. It is here for history reasons.
 *
 * This class original functionality was to store page information and do
 * word scoring. It was broking SRP and was replaces by {@link ie.gmit.sw.webcrowler.models.Page}
 * and {@link ie.gmit.sw.webcrowler.processors.PageProcessor} classes.
 *
 * @deprecated for removal.
 */
@Slf4j
public class PageData {
    public static final Predicate<String> nonEmpty = s -> !s.isEmpty();
    @Getter
    private final String ownerLink;
    @Getter
    private List<String> hrefs;
    @Getter
    private Flux<Word> wordsAsync;
    private int score = -1;

    private PageData(String link, List<String> hrefs, Flux<Word> wordsAsync) {
        this.ownerLink = link;
        this.hrefs = hrefs;
        this.wordsAsync = wordsAsync
//                .doOnNext(it -> log.info("{} - {}", it, link))
                ;
        log.info("Created {} for: {}", PageData.class.getName(), link);
    }

    public int getScore() {
        if(score < 0){
            score = wordsAsync
                    .map(Word::getTotalScore)
                    .reduce(Integer::sum)
                    .block();
        }

        return score;
    }

    public Mono<Integer> calculateScore(){
        return wordsAsync
                .map(Word::getTotalScore)
                .reduce(Integer::sum)
                .doOnNext(it -> score = it);
    }

    public Flux<TrieKeyValuePair<Integer>> getScoredWords(){
        var counter = new AtomicInteger();
        var limit = 20;
        return wordsAsync
                .reduce(TrieMap.empty(), (TrieMap<Integer>trie, Word word) ->{
                    trie.updateOrInsert(word.getWord(), word.getTotalScore(), Integer::sum);
                    return trie;
                })
                .map(Trie::collectKeyValuePairs)
                .flatMapMany(Flux::fromIterable)
                .sort((pair1, pair2) -> pair2.getValue() -pair1.getValue())
                .doOnNext(it -> counter.incrementAndGet())
                .takeWhile(pair ->
                        pair.getValue() > 5
                        || counter.get() < limit
                )
//                .log()
                ;
    }

    public boolean isScoreMoreThan(int minScore){
        return score > minScore;
    }

    public boolean isScoreMoreThanZero(){
        return isScoreMoreThan(0);
    }

    @Override
    public String toString() {
        return "LinkData{" +
                "link='" + ownerLink + '\'' +
//                ", hrefs=" + hrefs +
                ", score=" + score +
                '}';
    }

    // Builder class
    public static class Builder {
        private static final String SPLIT_ON = " ";

        private final QueryWord query;
//        private final TrieSet queryWordTrie;
        private final String link;
        private final IgnoreWords ignoreWords;
        private final FuzzyPageBooster fuzzy;

        private Flux<String> metaKeywords;
        private Flux<String> metaDescription;
        private Flux<String> title;
        private Flux<String> h1TagsText;
        private Flux<String> h2h3TagsText;
        private Flux<String> h4h5TagsText;
        private Flux<String> strongText;
        private Flux<String> pTagsText;
        private List<String> hrefs;

        private Builder(QueryWord queryWord, String link, IgnoreWords ignoreWords, FuzzyPageBooster fuzzy) {
            this.query = queryWord;
//            this.queryWordTrie = TrieSet.empty();
//            this.queryWordTrie.add(queryWord);
            this.link = link;
            this.ignoreWords = ignoreWords;
            this.fuzzy = fuzzy;
        }

        public static Builder of(QueryWord queryWord, String link, IgnoreWords ignoreWords, FuzzyPageBooster fuzzy) {
            return new Builder(queryWord, link, ignoreWords, fuzzy);
        }

        public Builder setHrefs(List<String> hrefs) {
            if(Objects.nonNull(hrefs)) {
                this.hrefs = hrefs.stream()
                        .filter(Objects::nonNull) // filter bad links
                        .collect(Collectors.toList())
                        ;
            }
            return this;
        }

        public Builder setMetaKeywords(String metaKeywords) {
            this.metaKeywords = filterText(metaKeywords);
            return this;
        }

        public Builder setMetaDescription(String metaDescription) {
            this.metaDescription = filterText(metaDescription);
            return this;
        }

        public Builder setTitle(String title) {
            this.title = filterText(title);
            return this;
        }

        public Builder setH1TagsText(String h1TagsText) {
            this.h1TagsText = filterText(h1TagsText);
            return this;
        }

        public Builder setH2h3TagsText(String h2h3TagsText) {
            this.h2h3TagsText = filterText(h2h3TagsText);
            return this;
        }

        public Builder setH4h5TagsText(String h4h5TagsText) {
            this.h4h5TagsText = filterText(h4h5TagsText);
            return this;
        }

        public Builder setStrongText(String strongText) {
            this.strongText = filterText(strongText);
            return this;
        }

        public Builder setpTagsText(String pTagsText) {
            this.pTagsText = filterText(pTagsText);
            var opt = Optional.ofNullable(this.pTagsText).orElse(Flux.empty());
            var wordsclose = WordProcessor.getWordsNearFreq(opt, query, 2);
            var wordsFrq = WordProcessor.getFrequentWords(opt);
            var queryWordCount = WordProcessor.getQueryWordCount(opt);

            return this;
        }

        public PageData build() {
            var metaKeywordsFrq = WordProcessor.getWordsNearFreq(Optional.ofNullable(metaKeywords).orElse(Flux.empty()), query, 1)
                    .map(pair -> createWordScored(pair, Word.Type.META_KEYWORD))
                    ;
            var metaDescripFrq = WordProcessor.getWordsNearFreq(Optional.ofNullable(metaDescription).orElse(Flux.empty()), query, 1)
                    .map(pair -> createWordScored(pair, Word.Type.META_DESCRIPTION))
                    ;
            var titleFrq = WordProcessor.getWordsNearFreq(Optional.ofNullable(title).orElse(Flux.empty()), query, 1)
                    .map(pair -> createWordScored(pair, Word.Type.TITLE))
                    ;
            var h1FrqWords = WordProcessor.getWordsNearFreq(Optional.ofNullable(h1TagsText).orElse(Flux.empty()), query, 1)
                    .map(pair -> createWordScored(pair, Word.Type.HEADER_1))
                    ;
            var h2h3FrqWords = WordProcessor.getWordsNearFreq(Optional.ofNullable(h2h3TagsText).orElse(Flux.empty()), query, 1)
                    .map(pair -> createWordScored(pair, Word.Type.HEADERS_2_3))
                    ;
            var h4h5FrqWords = WordProcessor.getWordsNearFreq(Optional.ofNullable(h4h5TagsText).orElse(Flux.empty()), query, 2)
                    .map(pair -> createWordScored(pair, Word.Type.HEADERS_4_5))
                    ;
            var highlightedFrqWords = WordProcessor.getWordsNearFreq(Optional.ofNullable(strongText).orElse(Flux.empty()), query, 2)
                    .map(pair -> createWordScored(pair, Word.Type.HIGHLIGHTED))
                    ;
            var paragraphFrqWords = WordProcessor.getWordsNearFreq(Optional.ofNullable(pTagsText).orElse(Flux.empty()), query, 2)
                    .map(pair -> createWordScored(pair, Word.Type.PARAGRAPH))
                    ;
//            var linkWords = WordProcessor.getFrequentWords(processLink(link))
//                    ;

            var linkScore = Flux.empty().concat(//linkWords,
                    metaKeywordsFrq, metaDescripFrq, titleFrq,
                    h1FrqWords, h2h3FrqWords, h4h5FrqWords,
                    highlightedFrqWords, paragraphFrqWords
            )
                    .filter(Word::nonEmpty)
                    .filter(Word::lengthInLimits)
//                    .doOnNext(it -> Runner.wordCounter.incrementAndGet())
//                    .filter(this::nonQueryWord)
//                    .doOnNext(System.out::println)
//                    .map(Word::getTotalScore)
//                    .reduce(Integer::sum)
//                    .subscribe(s -> System.out.println(link +" = "+ s))
                    ;

            return new PageData(link, hrefs, linkScore); // new LinkData(link, hrefs);
        }

        private boolean nonQueryWord(Word word){
            return !query.getTrieWord().contains(word.getWord());
        }

        private Word createWordScored(TrieKeyValuePair<Integer> pair, Word.Type type) {
//            System.out.println(pair);
            var w = pair.getKey();
            var frq = pair.getValue();
            var word = Word.of(w, type);
            word.addToScore(frq * type.getScore());
            return word;
        }

        private Flux<String> filterText(String text){
            if(Objects.isNull(text)){
                return Flux.empty();
            }
            return Mono.just(text)
                    .map(s -> s.replaceAll("-", SPLIT_ON))
                    .map(query::replace)
                    .map(s -> s.split(SPLIT_ON))
                    .flatMapMany(Flux::fromArray)
                    .map(String::toLowerCase)
                    .map(word -> word.replaceAll("[^a-z]", ""))
                    .filter(nonEmpty)
                    .filter(ignoreWords::notContains)
                    ;
        }

        private Flux<String> processLink(String link){
            var cleanLink = link.toLowerCase()
                    .replaceAll("(-|.|/)", " ")
                    ;
            var contains = cleanLink.contains(query.getQuery());

            if(contains){

            }

            return Flux.empty();
        }



        @Override
        public String toString() {
            return "Builder{" +
                    "\n\tlink='" + link + '\'' +
                    ",\n\threfs=" + hrefs +
//                    ",\n\tmetaKeywords=" + metaKeywords +
//                    ",\n\tmetaDescription=" + metaDescription +
//                    ",\n\ttitle=" + title +
//                    ",\n\th1TagsText=" + h1TagsText +
//                    ",\n\th2h3TagsText=" + h2h3TagsText +
//                    ",\n\th4h5TagsText=" + h4h5TagsText +
//                    ",\n\tstrongText=" + strongText +
//                    ",\n\tpTagsText=" + pTagsText +
                    '}';
        }


    }
}
