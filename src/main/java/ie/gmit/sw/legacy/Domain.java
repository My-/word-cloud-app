package ie.gmit.sw.legacy;

import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * NOTE: This is legacy code. It is here for history reasons.
 *
 * This class original idea was to get domain from the link and use it to
 * sort/filter links depending on it.
 *
 * @deprecated for removal.
 */
@Data
@Slf4j
public class Domain {
    public static final int CAPACITY = 100;
    private final String domainName;
    private final LinkedBlockingQueue<String> urls;

    public static Domain from(@NonNull String url){
        var domain = Domain.getDomainName(url);
        var self = new Domain(domain, new LinkedBlockingQueue<>(CAPACITY));
        self.urls.offer(url);

        return self;
    }

    public static String getDomainName(String url){
        if( url.trim().isEmpty()){
            log.warn("'url' is empty!");
            return "";
        }
        int startDomainIndex = url.indexOf("//") +2;
        int endDomainIndex = url.indexOf("/", startDomainIndex);
        var domainString = startDomainIndex < endDomainIndex
                ? url.substring(startDomainIndex, endDomainIndex)
                : url.substring(startDomainIndex)
                ;
        int extensionIndex = domainString.lastIndexOf(".");
        int domainStartIndex = domainString.lastIndexOf(".", extensionIndex -1);
        return domainStartIndex < 0
                ? domainString
                : domainString.substring(domainStartIndex +1)
                ;
    }

    public Domain merge(Domain domain) {
        if(this.domainName.equals(domain.domainName)){
            this.urls.addAll(domain.urls);  // throws is full
        }
        else{
            log.warn("Not ({}) domain. Got: {}", this.domainName, domain.domainName);
        }

        return this;
    }

    public Domain addUrl(@NonNull String url){
        var domainName = Domain.getDomainName(url);
        if( this.domainName.equals(domainName)){
            this.urls.add(url); // throws if full
        }
        else{
            log.warn("Not ({}) domain: {}", this.domainName, url);
        }

        return this;
    }

    public Optional<String> getUrl(int waitMils) throws InterruptedException {
        return Optional.ofNullable(this.urls.poll(waitMils, TimeUnit.MILLISECONDS));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Domain)) return false;
        Domain domain = (Domain) o;
        return this.domainName.equals(domain.domainName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainName);
    }
}
