package ie.gmit.sw.legacy;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

/**
 * NOTE: This is legacy code. It is here for history reasons.
 *
 * This class original functionality was to store scored word, but
 * not in use as {@link ie.gmit.sw.ds.trie.TrieKeyValuePair} was already
 * used inside pipelines and to convert to this class jus for a sake of
 * using it made no sense.
 *
 * @deprecated for removal or upgrade.
 */
@Data
@Slf4j
public class Word {
    public static final int MIN_LENGTH = 3;
    public static final int MAX_LENGTH = 12;

    private final CharSequence word;
    private final Type type;
    @EqualsAndHashCode.Exclude
    private int totalScore = 0;

    public static Word of(CharSequence word, Type type){
        return new Word(word, type);
    }

    public Word addToScore(int n){
        totalScore += n;
        return this;
    }

    public boolean lengthInLimits(){
        var length = word.length();
        return MIN_LENGTH < length && length < MAX_LENGTH;
    }

    public boolean nonEmpty(){
        return !word.toString().isEmpty();
    }

    // enum to hold word type and score
    enum Type {
        LINK_TEXT,
        META_KEYWORD,
        META_DESCRIPTION,
        TITLE,
        HEADER_1,
        HEADERS_2_3,
        HEADERS_4_5,
        HIGHLIGHTED,
        PARAGRAPH;

        public int getScore(){
            switch (this){
                case LINK_TEXT: return 53;
                case META_KEYWORD: return 43;
                case META_DESCRIPTION: return 37;
                case TITLE: return 29;
                case HEADER_1: return 19;
                case HEADERS_2_3: return 13;
                case HEADERS_4_5: return 7;
                case HIGHLIGHTED: return 3;
                case PARAGRAPH: return 1;
                default: return 0;
            }
        }
    }


}
