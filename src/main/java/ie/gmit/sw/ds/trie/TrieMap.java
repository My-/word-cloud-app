package ie.gmit.sw.ds.trie;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;

public class TrieMap<V> extends Trie<V> implements ConcurrentMap<CharSequence, V> {


    /**
     * Static factory method to create empty trie.
     *
     * @param newRoot
     * @return newly created trie with single empty
     * node which all bits set to `0`
     */
    TrieMap(TrieNode<V> newRoot) {
        super(newRoot);
    }

    public static <V>TrieMap<V> empty(){
        return new TrieMap<>(TrieNode.empty());
    }


    @Override
    public boolean containsKey(Object o) {
        if(o instanceof CharSequence){
            var cs = (CharSequence)o;
            return super.contains(cs, null, 0);
        }
        return false;
    }

    @Override
    public boolean containsValue(Object o) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public V get(Object o) {
        return super.getValue((CharSequence) o, 0);
    }

    @Override
    public V put(CharSequence sequence, V value) {
        super.insert(sequence, value);
        return super.getValue(sequence, 0);
    }

    public V put(TrieKeyValuePair<V> pair) {
        super.insert(pair.getKey(), pair.getValue());
        return super.getValue(pair.getKey(), 0);
    }

    @Override
    public V remove(Object o) {
        return super.removeValue((CharSequence)o);
    }

    @Override
    public void putAll(Map<? extends CharSequence, ? extends V> map) {
        map.forEach(this::put);
    }

    @Override
    public Set<CharSequence> keySet() {
        return Set.copyOf(super.keys());
    }

    @Override
    public Collection<V> values() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Set<Entry<CharSequence, V>> entrySet() {
        throw new UnsupportedOperationException("not implemented: use Trie.collectKeyValuePairs()");
    }

    @Override
    public  V putIfAbsent(CharSequence key, V value) {
        if(!this.containsKey(key)){
            return this.put(key, value);
        }
        else {
            return this.get(key);
        }
    }

    @Override
    public boolean remove(Object o, Object o1) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public  boolean replace(CharSequence key, V oldValue, V newValue) {
        if (this.containsKey(key)
                && Objects.equals(this.get(key), oldValue)) {
            this.put(key, newValue);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public V replace(CharSequence sequence, V v) {
        throw new UnsupportedOperationException("not implemented");
    }
}
