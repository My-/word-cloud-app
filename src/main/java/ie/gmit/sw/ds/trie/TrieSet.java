package ie.gmit.sw.ds.trie;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class TrieSet extends Trie<Boolean> implements Set<CharSequence> {
    public TrieSet(TrieNode<Boolean> root) {
        super(root);
    }

    public static TrieSet empty(){
        return new TrieSet(TrieNode.empty());
    }

    public static TrieSet of(String ...words) {
        var instance = TrieSet.empty();
        instance.addAll(Arrays.asList(words));
        return instance;
    }

    @Override
    public int size() {
        return keys().size();
    }

    @Override
    public boolean isEmpty() {
        return super.getRoot().getChildren().isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        // https://stackoverflow.com/questions/4001938/why-do-i-get-illegal-generic-type-for-instanceof
        if(o instanceof CharSequence){
            return contains((CharSequence) o, 0);
        }
        System.out.println("Not an object: "+ o);
        return false;
    }

    public boolean contains(CharSequence cs){
        return this.contains(cs, 0);
    }


    public boolean contains(CharSequence cs, int allowedMisses) {
        var value = super.getValue(cs, allowedMisses);
        if(Objects.nonNull(value)){
            return value;
        }
        return false;
    }

    @Override
    public Iterator<CharSequence> iterator() {
        return keys().iterator();
    }

    @Override
    public CharSequence[] toArray() {
        return keys().toArray(new CharSequence[size()]);
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return keys().toArray(ts);
    }

    @Override
    public boolean add(@NonNull CharSequence cs) {
        var res = this.insert(cs, true);
        if(Objects.isNull(res)){
            log.warn("failed to insert: {}", cs);
            System.out.println(cs.toString());
        }
        return Objects.nonNull(res);
    }

    @Override
    public boolean remove(Object o) {
        if(o instanceof CharSequence){
            CharSequence cs = (CharSequence)o;
            var value = super.removeValue(cs);
            return Objects.nonNull(value);
        }
        System.out.println("not an object: "+ o);
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return collection.stream()
                .allMatch(this::contains)
                ;
    }

    @Override
    public boolean addAll(Collection<? extends CharSequence> collection) {
        collection
                .forEach(cs -> this.insert(cs, true))
                ;
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        collection.forEach(this::remove);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        Function<Object, String> newString = o -> o instanceof String ? (String)o : "";
        List<String> elements = collection.stream()
                .map(newString)
                .filter(String::isEmpty)
                .filter(this::contains)
                .collect(Collectors.toList())
                ;

        this.clear();
        return this.addAll(elements);
    }

    @Override
    public void clear() {
        super.getRoot().dropChildren();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
