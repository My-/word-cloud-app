package ie.gmit.sw.ds.trie;

import lombok.Data;
import reactor.util.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.BiFunction;

/**
 * This class is {@link Trie} data structures node.
 * This class should be thread safe (note: not well tested).
 * To reduce memory usage this class uses 32 bit {@code int} to represent
 * 32 unique values {@link Trie} can store. Children node is
 * added then corresponding bit is set. Future memory reductions
 * can be made by replacing {@link TrieNode#children} collection
 * to the one which used primitive {@code int} instead of currently
 * used {@link Integer}. But currently Java collections doesnt
 * support it.
 */
public class TrieNode<V> {
    private int valueBitMap;
    private V endNodeValue;
    private final ConcurrentMap<Integer, TrieNode<V>> children;

    private TrieNode(ConcurrentMap<Integer, TrieNode<V>> children){
        this.children = children;
    }

    /**
     * Static factory method to create empty node.
     * @return newly created node with all bits set to `0`
     */
    public static <V>TrieNode <V>empty(){
        return new TrieNode<>(new ConcurrentSkipListMap<>());
    }

    public ConcurrentMap<Integer, TrieNode<V>> getChildren(){
        return this.children;
    }

    public V getEndNodeValue(){
        return this.endNodeValue;
    }

    public void setEndNodeValue(V value){
        this.endNodeValue = value;
    }

    public TrieNode<V> clearNodeValue(){
        this.endNodeValue = null;
        if(this.isLeafNode()){
            // ToDo: remove values till next end node or node with more then one child.
        }
        return this;
    }

    /**
     * Sets bit and creates node.
     * Delegates: {@link ConcurrentMap#compute(Object, BiFunction)}.
     * @param bitPos bit position we want to create node
     * @return the new value associated with the specified key, or null if none.
     */
    public @Nullable TrieNode<V> setBit(int bitPos){
        var bitValue = bitValueOf(bitPos);
        if((valueBitMap & bitValue) != bitValue){
            valueBitMap |= bitValue;
            return children.compute(bitValue, this::createOrThrowIfExist);
        }
        return getBitNode(bitPos);
    }

    /**
     * Get child node for a given bit position.
     * Delegates: {@link ConcurrentMap#get(Object)}
     * @param bitPos the node we want to get.
     * @return node at given position of null if not exist..
     */
    public @Nullable TrieNode<V> getBitNode(int bitPos){
        var bitValue = bitValueOf(bitPos);
        return children.get(bitValue);
    }

    /**
     * Checks if bit is set at given position.
     * O(1)
     * @param bitPos position of the bit.
     * @return true if bit is set at given position.
     */
    public boolean isBitSet(int bitPos){
        var bitValue = bitValueOf(bitPos);
        return (valueBitMap & bitValue) == bitValue;
    }

    /**
     * Checks if current node is a end node.
     * Note: it doesnt mean it is the leaf node.
     * Note: to check if it is a leaf node use: {@link TrieNode#isLeafNode()}.
     * O(1)
     * @return end node value
     */
    public boolean isEndNode(){
        return Objects.nonNull(endNodeValue);
    }

    /**
     * Check if current node is a leaf (last) node in the tree.
     * O(1)
     * @return
     */
    public boolean isLeafNode(){
        return valueBitMap == 0;
    }

    /**
     * Remove all children.
     * Delagates: {@link ConcurrentMap#clear()}
     */
    void dropChildren(){
        children.clear();
    }

    /**
     * Checks if node has children.
     * @return
     */
    public boolean hasChildren() {
        return !children.isEmpty();
    }


    private int bitValueOf(int bit){
        return (int)Math.pow(2, bit);
    }

    private TrieNode<V> createOrThrowIfExist(Integer k, TrieNode<? super V> v){
        if( Objects.nonNull(v)){
            throw new RuntimeException("It should have not existing value, but was: "+ v);
        }
        return TrieNode.empty();
    }

    @Override
    public String toString() {
        return "TrieNode{endNode: " +
                (Objects.nonNull(endNodeValue) ? "x" : "-") +
                ", valueBitMap=" + valueBitMap +
                ",\n\t children=" + children +
                "}";
    }


}
