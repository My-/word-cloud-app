package ie.gmit.sw.ds.trie;

import java.util.Objects;

public class TrieKeyValuePair<V> {
    private final CharSequence key;
    private final V value;

    public TrieKeyValuePair(CharSequence key, V value){
        this.key = key;
        this.value = value;
    }

    public static <V> TrieKeyValuePair<V> of(CharSequence key, V value){
        return new TrieKeyValuePair<>(key, value);
    }

    public CharSequence getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrieKeyValuePair)) return false;
        TrieKeyValuePair<?> that = (TrieKeyValuePair<?>) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public String toString() {
        return "KeyValuePair{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}
