package ie.gmit.sw.ds.trie;

import java.util.*;
import java.util.function.BiFunction;

/**
 * Trie is a tree like data structure.
 * This class implements Java {@link Set} interface.
 * This class uses {@link TrieNode} as a node class.
 * This class should be thread safe (not guaranteed).
 */

public abstract class Trie<V>
//        implements Set<CharSequence>
{
    private final TrieNode<V> root;

    /**
     * Static factory method to create empty trie.
     * @return newly created trie with single empty
     * node which all bits set to `0`
     */
//    public static Trie empty(){
//        return new Trie(TrieNode.empty());
//    }
    Trie(TrieNode<V> newRoot){
        this.root = newRoot;
    }

    public TrieNode<V> getRoot(){
        return this.root;
    }

    /**
     * Insert {@link String} to the trie.
     * @param key text to insert
     * @param value to insert
     * @return this instance
     */
    public V insert(CharSequence key, V value){
        var res = insert(key, 0, root, value);

        return res;
    }


    private V insert(CharSequence key, int pos, TrieNode<V> node, V value){
        if(pos == key.length()){
            node.setEndNodeValue(value);
            return node.getEndNodeValue();
        }
        if(Objects.isNull(node)){
            throw new NullPointerException("Got null node! key: "+key +" pos: "+ pos);
        }
        var newNode = node.setBit(key.charAt(pos) -'a');

        return insert(key, pos +1, newNode, value);
    }

    public V updateOrInsert(CharSequence key, V newValue, BiFunction<V, V, V> updater){
        var res = updateOrInsert(key, 0, newValue, updater, root);
        if(Objects.isNull(res)){
            return insert(key, newValue);
        }
        return res;
    }

    private V updateOrInsert(CharSequence key, int pos, V newValue, BiFunction<V, V, V> updater, TrieNode<V> node){
        if(pos == key.length()){
            if(node.isEndNode()){
                var oldValue = node.getEndNodeValue();
                var updatedValue = Objects.isNull(oldValue) ? newValue : updater.apply(oldValue, newValue);
                node.setEndNodeValue(updatedValue);
                return node.getEndNodeValue();
            }

            node.setEndNodeValue(newValue);
            return node.getEndNodeValue();
        }
        var newNode = node.getBitNode(key.charAt(pos) -'a');
        if(Objects.isNull(newNode)){
            // no node exist so create it
            newNode = node.setBit(key.charAt(pos) -'a');
        }
        return updateOrInsert(key, pos +1, newValue, updater, newNode);
    }

    /**
     * Checks if trie contain given {@link String} with allowing
     * given amount od misses.
     * @param key we testing.
     * @param allowedMisses allowed misses
     * @return true if sting exist with up to given misses
     */
    public boolean contains(CharSequence key, V value, int allowedMisses){
        var v = getValue(key, 0, allowedMisses, root);
        if(Objects.isNull(v)){
            return false;
        }
        return v.equals(value);
    }

    public boolean contains(CharSequence key){
        var v = getValue(key, 0, 0, root);
        return Objects.nonNull(v);
    }

    public V getValue(CharSequence key, int allowMisses){
        return getValue(key, 0, allowMisses, root);
    }

    private V getValue(CharSequence key, int pos, int ignoreLast, TrieNode<V> node) {
        if(pos == key.length()){
            if(node.isEndNode()){
                return node.getEndNodeValue();
            }
            return null;
        }
        var newNode = node.getBitNode(key.charAt(pos) -'a');
        if(Objects.isNull(newNode)){
            if(ignoreLast < 1){
                return null;
            }
            Collection<TrieNode<V>> children = node.getChildren().values();
            if(children.isEmpty()){
                if( key.length() -(pos +1) < ignoreLast){
                    return node.getEndNodeValue();
                }
                return null;
            }

            return children.stream()
                    .map(n -> getValue(key, pos +1, ignoreLast -1, n))
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null)
                    ;

        }
        return getValue(key, pos +1, ignoreLast, newNode);
    }

    /**
     * Removes value from the Trie
     * @param key
     * @return
     */
    public V removeValue(CharSequence key){
        return remove(key, 0, root);
    }

    private V remove(CharSequence key, int startPos, TrieNode<V> node){
        var l = key.length();
        V value = null;
        if(l == startPos){
            if(node.isEndNode()){
                value = node.getEndNodeValue();
                node.clearNodeValue();
                if(node.getChildren().isEmpty()){
                    // todo:prune this node
                }
                return value;
            }
            return null;
        }

        var newNode = node.getBitNode(key.charAt(startPos) -'a');
        if(Objects.isNull(newNode)){
            Collection<TrieNode<V>> children = node.getChildren().values();
            if(children.isEmpty()){
                return null;
            }

            return children.stream()
                    .map(n -> remove(key, startPos +1,  n))
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null)
                    ;
        }
        return remove(key, startPos +1,  newNode);
    }

    /**
     * Gets all values from the Trie.
     * @return
     */
    public Collection<CharSequence> keys(){
        var keys = new LinkedList<CharSequence>();
        collectKeys(root, new StringBuilder(), keys);
        return keys;
    }

    private void collectKeys(TrieNode<V> parentNode, StringBuilder key, Collection<CharSequence> keys){
        if(parentNode.isEndNode()){
            keys.add(key.toString());
            if(parentNode.isLeafNode()){
                key.deleteCharAt(key.length() -1);
                return;
            }
        }
        var keySet = parentNode.getChildren().keySet();
        for(int k : keySet){
            key.append((char)(binlog(k) + 'a'));
            var node = parentNode.getChildren().get(k);
            collectKeys(node, key, keys);
        }
        var lastIndex = key.length() -1;
        if(lastIndex < 0){
            return;
        }
        key.deleteCharAt(lastIndex);
    }

    public Collection<TrieKeyValuePair<V>> collectKeyValuePairs(){
        var pairs = new LinkedList<TrieKeyValuePair<V>>();
        collectKeyValuePairs(root, new StringBuilder(), pairs);
        return pairs;
    }

    private void collectKeyValuePairs(TrieNode<V> parentNode, StringBuilder key, Collection<TrieKeyValuePair<V>> pairs){
        if(parentNode.isEndNode()){
            var pair = TrieKeyValuePair.of(key.toString(), parentNode.getEndNodeValue());
            pairs.add(pair);
            if(parentNode.isLeafNode()){
                if(key.length() -1 < 0){
                    return;
                }
                key.deleteCharAt(key.length() -1);
                return;
            }
        }
        var keySet = parentNode.getChildren().keySet();
        for(int k : keySet){
            key.append((char)(binlog(k) + 'a'));
            var node = parentNode.getChildren().get(k);
            collectKeyValuePairs(node, key, pairs);
        }
        var lastIndex = key.length() -1;
        if(lastIndex < 0){
            return;
        }
        key.deleteCharAt(lastIndex);
    }

    // https://stackoverflow.com/a/3305710/5322506
    private static int binlog( int bits ) // returns 0 for bits=0
    {
        int log = 0;
        if( ( bits & 0xffff0000 ) != 0 ) { bits >>>= 16; log = 16; }
        if( bits >= 256 ) { bits >>>= 8; log += 8; }
        if( bits >= 16  ) { bits >>>= 4; log += 4; }
        if( bits >= 4   ) { bits >>>= 2; log += 2; }
        return log + ( bits >>> 1 );
    }

    @Override
    public String toString() {
        return "Trie{" +
                "root=" + root +
                '}';
    }
//    @Override
    public int size() {
        return keys().size();
    }

//    @Override
    public boolean isEmpty() {
        return root.getChildren().isEmpty();
    }

//    @Override
    public CharSequence[] toArray() {
        return keys().toArray(new CharSequence[size()]);
    }

//    @Override
    public <T> T[] toArray(T[] ts) {
        return keys().toArray(ts);
    }




    public void clear() {
        root.dropChildren();
    }
}
